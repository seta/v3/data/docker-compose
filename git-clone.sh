#!/bin/bash

cd ../
git clone git@code.europa.eu:seta/v3/data/data-repos.git
git clone git@code.europa.eu:seta/v3/data/ingest-service.git
git clone git@code.europa.eu:seta/v3/data/nlp-service.git
git clone git@code.europa.eu:seta/v3/data/workflow.git

mkdir -p harvest
cd harvest
git clone git@code.europa.eu:seta/v3/data/harvest/harvest-cellar.git