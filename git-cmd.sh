#!/bin/bash

cd ../data-repos
echo data-repos
git "$@"

cd ../docker-compose
echo docker-compose
git "$@"

cd ../ingest-service
echo ingest-service
git "$@"

cd ../nlp-service
echo nlp-service
git "$@"

cd ../workflow
echo workflow
git "$@"

cd ../harvest/harvest-cellar
echo harvest-cellar
git "$@"
