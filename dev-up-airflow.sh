#!/bin/bash

docker compose -f ./airflow/docker-compose-dev.yml --env-file ./airflow/.env.dev up $@