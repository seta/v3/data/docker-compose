# docker-compose

## Build Images

On __Linux__, make bash files executable:
```
chmod +x build-images.sh
chmod +x push-images.sh
```

Build images with docker compose:
```
./build-images.sh
```

Or only specific one:
```
./build-images.sh postgres
```

Push images with docker compose:
```
./push-images.sh
```

Or only specific one:
```
./push-images.sh postgres
```

## Production Composer

### Create mounted directories
On __Linux__,  the quick-start needs to know your host user id and needs to have group id set to 0.

```
echo -e "AIRFLOW_UID=$(id -u)" > .env
```

For other operating systems, add to the `.env` file:
```
AIRFLOW_UID=50000
```

### Initialize the database
```
docker compose up airflow-init
```

The script will create local folder `./logs` (_git ignored_) that contains logs from task execution and scheduler.

### Start all services
```
docker compose up
```

### Cleaning-up the environment

```
docker compose down --volumes --remove-orphans
```

### Setup

Save the private key to `./private/id.rsa` file - check `PRIVATE_KEY_PATH` value in ingest-service `config/config/py` file.
The registered user should have 'Data Owner' permission for ingested data sources.

### Docker Compose Run

Harvest Cellar:
```
docker compose run --rm harvest-cellar import.py --lang eng --langISO2 en --import_dir <path>

docker compose run --rm harvest-cellar download.py --lang eng --langISO2 en

docker compose run --rm harvest-cellar format_ingest.py
```

Ingest Service:

```
docker compose run --rm ingest ingest.py
```