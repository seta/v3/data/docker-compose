#!/bin/bash

docker compose -f ./airflow/docker-compose-dev.yml --env-file ./airflow/.env.dev build

docker compose -f ./services/docker-compose-dev.yml --env-file ./services/.env.dev build